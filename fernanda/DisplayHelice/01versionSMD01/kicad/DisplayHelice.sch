EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x15_Female J1
U 1 1 60786250
P 4100 2500
F 0 "J1" V 4173 2480 50  0000 C CNN
F 1 "Nano02" V 4264 2480 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 4100 2500 50  0001 C CNN
F 3 "~" H 4100 2500 50  0001 C CNN
	1    4100 2500
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x15_Female J2
U 1 1 607880F0
P 4100 3000
F 0 "J2" V 4265 2980 50  0000 C CNN
F 1 "Nano01" V 4174 2980 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 4100 3000 50  0001 C CNN
F 3 "~" H 4100 3000 50  0001 C CNN
	1    4100 3000
	0    1    -1   0   
$EndComp
Text GLabel 3400 3300 3    50   Input ~ 0
Signal
Text GLabel 4800 3300 3    50   Input ~ 0
VIN
Text GLabel 4700 3300 3    50   Input ~ 0
GND
Wire Wire Line
	3400 3300 3400 3200
Wire Wire Line
	4500 3300 4500 3200
Wire Wire Line
	4700 3200 4700 3300
Wire Wire Line
	4800 3300 4800 3200
Text GLabel 3400 2200 1    50   Input ~ 0
D12
Text GLabel 3500 2200 1    50   Input ~ 0
D11
Text GLabel 3600 2200 1    50   Input ~ 0
D10
Text GLabel 3700 2200 1    50   Input ~ 0
D9
Text GLabel 3800 2200 1    50   Input ~ 0
D8
Text GLabel 3900 2200 1    50   Input ~ 0
D7
Text GLabel 4000 2200 1    50   Input ~ 0
D6
Text GLabel 4100 2200 1    50   Input ~ 0
D5
Text GLabel 4200 2200 1    50   Input ~ 0
D4
Text GLabel 4300 2200 1    50   Input ~ 0
D3
Text GLabel 4400 2200 1    50   Input ~ 0
D2
Text GLabel 4500 2200 1    50   Input ~ 0
GND
Wire Wire Line
	4500 2200 4500 2300
Wire Wire Line
	4300 2200 4300 2300
Wire Wire Line
	4200 2300 4200 2200
Wire Wire Line
	4100 2200 4100 2300
Wire Wire Line
	4000 2300 4000 2200
Wire Wire Line
	3900 2200 3900 2300
Wire Wire Line
	3800 2300 3800 2200
Wire Wire Line
	3700 2200 3700 2300
Wire Wire Line
	3600 2300 3600 2200
Wire Wire Line
	3500 2200 3500 2300
Wire Wire Line
	3400 2300 3400 2200
$Comp
L Device:LED D1
U 1 1 60791260
P 6000 2400
F 0 "D1" H 5993 2617 50  0000 C CNN
F 1 "LED" H 5993 2526 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 2400 50  0001 C CNN
F 3 "~" H 6000 2400 50  0001 C CNN
	1    6000 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 60791371
P 6000 2700
F 0 "D2" H 5993 2917 50  0000 C CNN
F 1 "LED" H 5993 2826 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 2700 50  0001 C CNN
F 3 "~" H 6000 2700 50  0001 C CNN
	1    6000 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 60791573
P 6000 3000
F 0 "D3" H 5993 3217 50  0000 C CNN
F 1 "LED" H 5993 3126 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 3000 50  0001 C CNN
F 3 "~" H 6000 3000 50  0001 C CNN
	1    6000 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 607916E9
P 6000 3300
F 0 "D4" H 5993 3517 50  0000 C CNN
F 1 "LED" H 5993 3426 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 3300 50  0001 C CNN
F 3 "~" H 6000 3300 50  0001 C CNN
	1    6000 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D5
U 1 1 607918FB
P 6000 3600
F 0 "D5" H 5993 3817 50  0000 C CNN
F 1 "LED" H 5993 3726 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 3600 50  0001 C CNN
F 3 "~" H 6000 3600 50  0001 C CNN
	1    6000 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D6
U 1 1 60793A67
P 6000 3900
F 0 "D6" H 5993 4117 50  0000 C CNN
F 1 "LED" H 5993 4026 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 3900 50  0001 C CNN
F 3 "~" H 6000 3900 50  0001 C CNN
	1    6000 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D7
U 1 1 60793A6D
P 6000 4200
F 0 "D7" H 5993 4417 50  0000 C CNN
F 1 "LED" H 5993 4326 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 4200 50  0001 C CNN
F 3 "~" H 6000 4200 50  0001 C CNN
	1    6000 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D8
U 1 1 60793A73
P 6000 4500
F 0 "D8" H 5993 4717 50  0000 C CNN
F 1 "LED" H 5993 4626 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 4500 50  0001 C CNN
F 3 "~" H 6000 4500 50  0001 C CNN
	1    6000 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D9
U 1 1 60793A79
P 6000 4800
F 0 "D9" H 5993 5017 50  0000 C CNN
F 1 "LED" H 5993 4926 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 4800 50  0001 C CNN
F 3 "~" H 6000 4800 50  0001 C CNN
	1    6000 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D10
U 1 1 60793A7F
P 6000 5100
F 0 "D10" H 5993 5317 50  0000 C CNN
F 1 "LED" H 5993 5226 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 5100 50  0001 C CNN
F 3 "~" H 6000 5100 50  0001 C CNN
	1    6000 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D11
U 1 1 60794E2E
P 6000 5400
F 0 "D11" H 5993 5617 50  0000 C CNN
F 1 "LED" H 5993 5526 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 5400 50  0001 C CNN
F 3 "~" H 6000 5400 50  0001 C CNN
	1    6000 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 607953E4
P 6450 2400
F 0 "R1" V 6243 2400 50  0000 C CNN
F 1 "R" V 6334 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 2400 50  0001 C CNN
F 3 "~" H 6450 2400 50  0001 C CNN
	1    6450 2400
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 60795CAE
P 6450 2700
F 0 "R2" V 6243 2700 50  0000 C CNN
F 1 "R" V 6334 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 2700 50  0001 C CNN
F 3 "~" H 6450 2700 50  0001 C CNN
	1    6450 2700
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 6079774A
P 6450 3000
F 0 "R3" V 6243 3000 50  0000 C CNN
F 1 "R" V 6334 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 3000 50  0001 C CNN
F 3 "~" H 6450 3000 50  0001 C CNN
	1    6450 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 60797750
P 6450 3300
F 0 "R4" V 6243 3300 50  0000 C CNN
F 1 "R" V 6334 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 3300 50  0001 C CNN
F 3 "~" H 6450 3300 50  0001 C CNN
	1    6450 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 6079A495
P 6450 3600
F 0 "R5" V 6243 3600 50  0000 C CNN
F 1 "R" V 6334 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 3600 50  0001 C CNN
F 3 "~" H 6450 3600 50  0001 C CNN
	1    6450 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 6079A4A1
P 6450 4200
F 0 "R7" V 6243 4200 50  0000 C CNN
F 1 "R" V 6334 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 4200 50  0001 C CNN
F 3 "~" H 6450 4200 50  0001 C CNN
	1    6450 4200
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 6079A4A7
P 6450 4500
F 0 "R8" V 6243 4500 50  0000 C CNN
F 1 "R" V 6334 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 4500 50  0001 C CNN
F 3 "~" H 6450 4500 50  0001 C CNN
	1    6450 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 6079CCF0
P 6450 4800
F 0 "R9" V 6243 4800 50  0000 C CNN
F 1 "R" V 6334 4800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 4800 50  0001 C CNN
F 3 "~" H 6450 4800 50  0001 C CNN
	1    6450 4800
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 6079CCF6
P 6450 5100
F 0 "R10" V 6243 5100 50  0000 C CNN
F 1 "R" V 6334 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 5100 50  0001 C CNN
F 3 "~" H 6450 5100 50  0001 C CNN
	1    6450 5100
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 6079CCFC
P 6450 5400
F 0 "R11" V 6243 5400 50  0000 C CNN
F 1 "R" V 6334 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 5400 50  0001 C CNN
F 3 "~" H 6450 5400 50  0001 C CNN
	1    6450 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	6300 5400 6150 5400
Wire Wire Line
	6150 5100 6300 5100
Wire Wire Line
	6300 4800 6150 4800
Wire Wire Line
	6150 4500 6300 4500
Wire Wire Line
	6300 4200 6150 4200
Wire Wire Line
	6150 3900 6300 3900
Wire Wire Line
	6300 3600 6150 3600
Wire Wire Line
	6150 3300 6300 3300
Wire Wire Line
	6300 3000 6150 3000
Wire Wire Line
	6150 2700 6300 2700
Wire Wire Line
	6300 2400 6150 2400
Wire Wire Line
	5850 2400 5650 2400
Wire Wire Line
	5650 2400 5650 2700
Wire Wire Line
	5650 5400 5850 5400
Connection ~ 5650 5400
Wire Wire Line
	5650 5400 5650 5750
Wire Wire Line
	5850 5100 5650 5100
Connection ~ 5650 5100
Wire Wire Line
	5650 5100 5650 5400
Wire Wire Line
	5650 4800 5850 4800
Connection ~ 5650 4800
Wire Wire Line
	5650 4800 5650 5100
Wire Wire Line
	5850 4500 5650 4500
Connection ~ 5650 4500
Wire Wire Line
	5650 4500 5650 4800
Wire Wire Line
	5650 4200 5850 4200
Connection ~ 5650 4200
Wire Wire Line
	5650 4200 5650 4500
Wire Wire Line
	5850 3900 5650 3900
Connection ~ 5650 3900
Wire Wire Line
	5650 3900 5650 4200
Wire Wire Line
	5650 3600 5850 3600
Connection ~ 5650 3600
Wire Wire Line
	5650 3600 5650 3900
Wire Wire Line
	5850 3300 5650 3300
Connection ~ 5650 3300
Wire Wire Line
	5650 3300 5650 3600
Wire Wire Line
	5650 3000 5850 3000
Connection ~ 5650 3000
Wire Wire Line
	5650 3000 5650 3300
Wire Wire Line
	5850 2700 5650 2700
Connection ~ 5650 2700
Wire Wire Line
	5650 2700 5650 3000
Text GLabel 5650 5750 3    50   Input ~ 0
GND
Text GLabel 6850 2400 2    50   Input ~ 0
D12
Text GLabel 6850 2700 2    50   Input ~ 0
D11
Text GLabel 6850 3000 2    50   Input ~ 0
D10
Text GLabel 6850 3300 2    50   Input ~ 0
D9
Text GLabel 6850 3600 2    50   Input ~ 0
D8
Text GLabel 6850 3900 2    50   Input ~ 0
D7
Text GLabel 6850 4200 2    50   Input ~ 0
D6
Text GLabel 6850 4500 2    50   Input ~ 0
D5
Text GLabel 6850 4800 2    50   Input ~ 0
D4
Text GLabel 6850 5100 2    50   Input ~ 0
D3
Text GLabel 6850 5400 2    50   Input ~ 0
D2
Wire Wire Line
	6850 2400 6600 2400
Wire Wire Line
	6600 2700 6850 2700
Wire Wire Line
	6850 3000 6600 3000
Wire Wire Line
	6600 3300 6850 3300
Wire Wire Line
	6850 3600 6600 3600
Wire Wire Line
	6600 3900 6850 3900
Wire Wire Line
	6850 4200 6600 4200
Wire Wire Line
	6600 4500 6850 4500
Wire Wire Line
	6850 4800 6600 4800
Wire Wire Line
	6600 5100 6850 5100
Wire Wire Line
	6850 5400 6600 5400
Wire Wire Line
	4400 2300 4400 2200
Wire Notes Line
	2750 1650 7500 1650
Wire Notes Line
	7500 1650 7500 6050
Wire Notes Line
	5250 1650 5250 6050
Wire Notes Line
	2750 3800 5250 3800
Wire Wire Line
	3700 5700 3850 5700
Wire Wire Line
	3400 5700 3400 5800
Text GLabel 4550 5700 2    50   Input ~ 0
VIN
Wire Wire Line
	4450 5700 4550 5700
$Comp
L Switch:SW_DIP_x01 SW1
U 1 1 60808609
P 4150 5700
F 0 "SW1" H 4150 5967 50  0000 C CNN
F 1 "SW" H 4150 5876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x01_P2.54mm_Vertical" H 4150 5700 50  0001 C CNN
F 3 "~" H 4150 5700 50  0001 C CNN
	1    4150 5700
	-1   0    0    1   
$EndComp
Text GLabel 3400 5800 3    50   Input ~ 0
GND
Wire Notes Line
	2750 5150 5250 5150
Text GLabel 4600 4450 2    50   Input ~ 0
Signal
Wire Wire Line
	3900 4900 3900 4850
Text GLabel 3900 4900 3    50   Input ~ 0
GND
Wire Wire Line
	4400 4450 4600 4450
Wire Wire Line
	4400 4450 4300 4450
Connection ~ 4400 4450
Wire Wire Line
	4400 4350 4400 4450
$Comp
L Device:R R12
U 1 1 607F134D
P 4400 4200
F 0 "R12" H 4470 4246 50  0000 L CNN
F 1 "R" H 4470 4155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 4330 4200 50  0001 C CNN
F 3 "~" H 4400 4200 50  0001 C CNN
	1    4400 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4050 4400 4050
Wire Wire Line
	3900 4000 3900 4050
Wire Notes Line
	7500 6050 2750 6050
$Comp
L Device:R R6
U 1 1 6079A49B
P 6450 3900
F 0 "R6" V 6243 3900 50  0000 C CNN
F 1 "R" V 6334 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6380 3900 50  0001 C CNN
F 3 "~" H 6450 3900 50  0001 C CNN
	1    6450 3900
	0    1    1    0   
$EndComp
Wire Notes Line
	2750 1650 2750 6050
$Comp
L Device:Battery_Cell BT1
U 1 1 607FF6D7
P 3500 5700
F 0 "BT1" V 3245 5750 50  0000 C CNN
F 1 "2sLiPo" V 3336 5750 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" V 3500 5760 50  0001 C CNN
F 3 "~" V 3500 5760 50  0001 C CNN
	1    3500 5700
	0    1    1    0   
$EndComp
$Comp
L DisplayHelice-rescue:A3144-hall U1
U 1 1 607EF8CC
P 4000 4450
F 0 "U1" H 3771 4496 50  0000 R CNN
F 1 "A3144" H 3771 4405 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4000 4100 50  0001 L CIN
F 3 "" H 4000 5100 50  0001 C CNN
	1    4000 4450
	1    0    0    -1  
$EndComp
Connection ~ 3900 4050
NoConn ~ 4600 2300
NoConn ~ 4700 2300
NoConn ~ 4800 2300
NoConn ~ 4600 3200
NoConn ~ 4400 3200
NoConn ~ 4300 3200
NoConn ~ 4200 3200
NoConn ~ 4100 3200
NoConn ~ 4000 3200
NoConn ~ 3500 3200
NoConn ~ 3600 3200
NoConn ~ 3700 3200
NoConn ~ 3800 3200
NoConn ~ 3900 3200
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 6080EBC9
P 1950 5500
F 0 "#FLG0101" H 1950 5575 50  0001 C CNN
F 1 "PWR_FLAG" H 1950 5673 50  0000 C CNN
F 2 "" H 1950 5500 50  0001 C CNN
F 3 "~" H 1950 5500 50  0001 C CNN
	1    1950 5500
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 6080F350
P 2350 5500
F 0 "#FLG0102" H 2350 5575 50  0001 C CNN
F 1 "PWR_FLAG" H 2350 5673 50  0000 C CNN
F 2 "" H 2350 5500 50  0001 C CNN
F 3 "~" H 2350 5500 50  0001 C CNN
	1    2350 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 608212D9
P 2350 5800
F 0 "#PWR0101" H 2350 5550 50  0001 C CNN
F 1 "GND" H 2355 5627 50  0000 C CNN
F 2 "" H 2350 5800 50  0001 C CNN
F 3 "" H 2350 5800 50  0001 C CNN
	1    2350 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 5500 2350 5800
$Comp
L power:GND #PWR0102
U 1 1 608245E5
P 1550 5800
F 0 "#PWR0102" H 1550 5550 50  0001 C CNN
F 1 "GND" H 1555 5627 50  0000 C CNN
F 2 "" H 1550 5800 50  0001 C CNN
F 3 "" H 1550 5800 50  0001 C CNN
	1    1550 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 5650 1550 5800
$Comp
L power:+5V #PWR0103
U 1 1 60829D99
P 4500 3300
F 0 "#PWR0103" H 4500 3150 50  0001 C CNN
F 1 "+5V" H 4515 3473 50  0000 C CNN
F 2 "" H 4500 3300 50  0001 C CNN
F 3 "" H 4500 3300 50  0001 C CNN
	1    4500 3300
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 6083D1D9
P 3900 4000
F 0 "#PWR0104" H 3900 3850 50  0001 C CNN
F 1 "+5V" H 3915 4173 50  0000 C CNN
F 2 "" H 3900 4000 50  0001 C CNN
F 3 "" H 3900 4000 50  0001 C CNN
	1    3900 4000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0105
U 1 1 60841FA7
P 1950 5800
F 0 "#PWR0105" H 1950 5650 50  0001 C CNN
F 1 "+5V" H 1965 5973 50  0000 C CNN
F 2 "" H 1950 5800 50  0001 C CNN
F 3 "" H 1950 5800 50  0001 C CNN
	1    1950 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	1950 5800 1950 5500
$Comp
L MCU_Microchip_ATmega:ATmega328-AU U2
U 1 1 607F0AD7
P 8950 3850
F 0 "U2" H 8950 2261 50  0000 C CNN
F 1 "ATmega328-AU" H 8950 2170 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 8950 3850 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 8950 3850 50  0001 C CNN
	1    8950 3850
	1    0    0    -1  
$EndComp
Text GLabel 1550 5650 1    50   Input ~ 0
GND
Text Notes 2850 6800 0    50   ~ 0
Para LED blanco\n\nV_on = 3 V\nI_f = 20 mA = 0.02 A\n\nR = 3 / 0.02 = 150 Ohms
$EndSCHEMATC
