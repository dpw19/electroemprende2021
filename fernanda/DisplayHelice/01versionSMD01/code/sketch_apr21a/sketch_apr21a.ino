#define DELAY 30
int ANALOG = 3;

void setup(){

    pinMode(ANALOG, OUTPUT);
    Serial.begin(19200);
    delay(100);
}

void loop(){
  for (int i = 1; i <= 254; i++){
    analogWrite(ANALOG, i);
    delay(DELAY);
    Serial.println(i);
  }

  for (int i= 254; i >= 1; i--){
    analogWrite(ANALOG, i);
    delay(DELAY);
    Serial.println(i);
  }
}
