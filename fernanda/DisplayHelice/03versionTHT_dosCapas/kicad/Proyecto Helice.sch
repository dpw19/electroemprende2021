EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x15_Female J1
U 1 1 608302C2
P 2900 2000
F 0 "J1" V 2973 1980 50  0000 C CNN
F 1 "Conn_01x15_Female" V 3064 1980 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x15_P2.54mm_Vertical" H 2900 2000 50  0001 C CNN
F 3 "~" H 2900 2000 50  0001 C CNN
	1    2900 2000
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x15_Female J2
U 1 1 608349CF
P 2900 2450
F 0 "J2" V 3065 2430 50  0000 C CNN
F 1 "Conn_01x15_Female" V 2974 2430 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x15_P2.54mm_Vertical" H 2900 2450 50  0001 C CNN
F 3 "~" H 2900 2450 50  0001 C CNN
	1    2900 2450
	0    1    -1   0   
$EndComp
Text GLabel 2200 1650 1    50   Input ~ 0
D12
Text GLabel 2300 1650 1    50   Input ~ 0
D11
Text GLabel 2400 1650 1    50   Input ~ 0
D10
Text GLabel 2500 1650 1    50   Input ~ 0
D9
Text GLabel 2600 1650 1    50   Input ~ 0
D8
Text GLabel 2700 1650 1    50   Input ~ 0
D7
Text GLabel 2800 1650 1    50   Input ~ 0
D6
Text GLabel 2900 1650 1    50   Input ~ 0
D5
Text GLabel 3000 1650 1    50   Input ~ 0
D4
Text GLabel 3100 1650 1    50   Input ~ 0
D3
Text GLabel 3200 1650 1    50   Input ~ 0
D2
Text GLabel 3300 1650 1    50   Input ~ 0
GND
Text GLabel 2200 2800 3    50   Input ~ 0
Signal
Text GLabel 3600 2800 3    50   Input ~ 0
VIN
Text GLabel 3500 2800 3    50   Input ~ 0
GND
Text GLabel 3300 2800 3    50   Input ~ 0
5V
Wire Wire Line
	3300 1800 3300 1650
Wire Wire Line
	3200 1650 3200 1800
Wire Wire Line
	3100 1800 3100 1650
Wire Wire Line
	3000 1650 3000 1800
Wire Wire Line
	2900 1800 2900 1650
Wire Wire Line
	2800 1650 2800 1800
Wire Wire Line
	2700 1800 2700 1650
Wire Wire Line
	2600 1650 2600 1800
Wire Wire Line
	2500 1800 2500 1650
Wire Wire Line
	2400 1650 2400 1800
Wire Wire Line
	2300 1800 2300 1650
Wire Wire Line
	2200 1650 2200 1800
Wire Wire Line
	2200 2650 2200 2800
Wire Wire Line
	3300 2800 3300 2650
Wire Wire Line
	3500 2650 3500 2800
$Comp
L Sensor_Magnetic:A1101ELHL U1
U 1 1 608385ED
P 2650 4400
F 0 "U1" H 2420 4446 50  0000 R CNN
F 1 "A1101ELHL" H 2420 4355 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2650 4050 50  0001 L CIN
F 3 "http://www.allegromicro.com/en/Products/Part_Numbers/1101/1101.pdf" H 2650 5050 50  0001 C CNN
	1    2650 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 6083B689
P 3250 4200
F 0 "R1" H 3320 4246 50  0000 L CNN
F 1 "R" H 3320 4155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 4200 50  0001 C CNN
F 3 "~" H 3250 4200 50  0001 C CNN
	1    3250 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 4000 3250 4000
Wire Wire Line
	3250 4000 3250 4050
Wire Wire Line
	3250 4350 3250 4400
Wire Wire Line
	3250 4400 2950 4400
Text GLabel 2550 3900 1    50   Input ~ 0
5V
Wire Wire Line
	2550 3900 2550 4000
Connection ~ 2550 4000
Text GLabel 3500 4400 2    50   Input ~ 0
Signal
Wire Wire Line
	3500 4400 3250 4400
Connection ~ 3250 4400
Text GLabel 2550 4800 3    50   Input ~ 0
GND
$Comp
L Switch:SW_DIP_x01 SW1
U 1 1 6083E581
P 2950 5800
F 0 "SW1" H 2950 5533 50  0000 C CNN
F 1 "SW_DIP_x01" H 2950 5624 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 2950 5800 50  0001 C CNN
F 3 "~" H 2950 5800 50  0001 C CNN
	1    2950 5800
	-1   0    0    1   
$EndComp
Text GLabel 3450 5800 2    50   3State ~ 0
VIN
Wire Wire Line
	3450 5800 3250 5800
$Comp
L Device:LED D1
U 1 1 6084673C
P 7300 1700
F 0 "D1" H 7293 1917 50  0000 C CNN
F 1 "LED" H 7293 1826 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 1700 50  0001 C CNN
F 3 "~" H 7300 1700 50  0001 C CNN
	1    7300 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 60846D27
P 7900 1700
F 0 "R2" V 7693 1700 50  0000 C CNN
F 1 "R" V 7784 1700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 1700 50  0001 C CNN
F 3 "~" H 7900 1700 50  0001 C CNN
	1    7900 1700
	0    1    1    0   
$EndComp
Text GLabel 8300 1700 2    50   3State ~ 0
D12
Wire Wire Line
	8300 1700 8050 1700
Wire Wire Line
	7450 1700 7750 1700
$Comp
L Device:LED D3
U 1 1 6084C171
P 7300 2300
F 0 "D3" H 7293 2517 50  0000 C CNN
F 1 "LED" H 7293 2426 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 2300 50  0001 C CNN
F 3 "~" H 7300 2300 50  0001 C CNN
	1    7300 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 6084C177
P 7900 2300
F 0 "R4" V 7693 2300 50  0000 C CNN
F 1 "R" V 7784 2300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 2300 50  0001 C CNN
F 3 "~" H 7900 2300 50  0001 C CNN
	1    7900 2300
	0    1    1    0   
$EndComp
Text GLabel 8300 2300 2    50   3State ~ 0
D10
Wire Wire Line
	8300 2300 8050 2300
Wire Wire Line
	7450 2300 7750 2300
$Comp
L Device:LED D4
U 1 1 6084C69F
P 7300 2600
F 0 "D4" H 7293 2817 50  0000 C CNN
F 1 "LED" H 7293 2726 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 2600 50  0001 C CNN
F 3 "~" H 7300 2600 50  0001 C CNN
	1    7300 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 6084C6A5
P 7900 2600
F 0 "R5" V 7693 2600 50  0000 C CNN
F 1 "R" V 7784 2600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 2600 50  0001 C CNN
F 3 "~" H 7900 2600 50  0001 C CNN
	1    7900 2600
	0    1    1    0   
$EndComp
Text GLabel 8300 2600 2    50   3State ~ 0
D9
Wire Wire Line
	8300 2600 8050 2600
Wire Wire Line
	7450 2600 7750 2600
$Comp
L Device:LED D5
U 1 1 6084CC63
P 7300 2900
F 0 "D5" H 7293 3117 50  0000 C CNN
F 1 "LED" H 7293 3026 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 2900 50  0001 C CNN
F 3 "~" H 7300 2900 50  0001 C CNN
	1    7300 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 6084CC69
P 7900 2900
F 0 "R6" V 7693 2900 50  0000 C CNN
F 1 "R" V 7784 2900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 2900 50  0001 C CNN
F 3 "~" H 7900 2900 50  0001 C CNN
	1    7900 2900
	0    1    1    0   
$EndComp
Text GLabel 8300 2900 2    50   3State ~ 0
D8
Wire Wire Line
	8300 2900 8050 2900
Wire Wire Line
	7450 2900 7750 2900
$Comp
L Device:LED D6
U 1 1 6084D318
P 7300 3200
F 0 "D6" H 7293 3417 50  0000 C CNN
F 1 "LED" H 7293 3326 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 3200 50  0001 C CNN
F 3 "~" H 7300 3200 50  0001 C CNN
	1    7300 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 6084D31E
P 7900 3200
F 0 "R7" V 7693 3200 50  0000 C CNN
F 1 "R" V 7784 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 3200 50  0001 C CNN
F 3 "~" H 7900 3200 50  0001 C CNN
	1    7900 3200
	0    1    1    0   
$EndComp
Text GLabel 8300 3200 2    50   3State ~ 0
D7
Wire Wire Line
	8300 3200 8050 3200
Wire Wire Line
	7450 3200 7750 3200
$Comp
L Device:LED D7
U 1 1 6084D981
P 7300 3500
F 0 "D7" H 7293 3717 50  0000 C CNN
F 1 "LED" H 7293 3626 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 3500 50  0001 C CNN
F 3 "~" H 7300 3500 50  0001 C CNN
	1    7300 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 6084D987
P 7900 3500
F 0 "R8" V 7693 3500 50  0000 C CNN
F 1 "R" V 7784 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 3500 50  0001 C CNN
F 3 "~" H 7900 3500 50  0001 C CNN
	1    7900 3500
	0    1    1    0   
$EndComp
Text GLabel 8300 3500 2    50   3State ~ 0
D6
Wire Wire Line
	8300 3500 8050 3500
Wire Wire Line
	7450 3500 7750 3500
$Comp
L Device:LED D8
U 1 1 6084DFD3
P 7300 3800
F 0 "D8" H 7293 4017 50  0000 C CNN
F 1 "LED" H 7293 3926 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 3800 50  0001 C CNN
F 3 "~" H 7300 3800 50  0001 C CNN
	1    7300 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 6084DFD9
P 7900 3800
F 0 "R9" V 7693 3800 50  0000 C CNN
F 1 "R" V 7784 3800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 3800 50  0001 C CNN
F 3 "~" H 7900 3800 50  0001 C CNN
	1    7900 3800
	0    1    1    0   
$EndComp
Text GLabel 8300 3800 2    50   3State ~ 0
D5
Wire Wire Line
	8300 3800 8050 3800
Wire Wire Line
	7450 3800 7750 3800
$Comp
L Device:LED D9
U 1 1 6084E78F
P 7300 4100
F 0 "D9" H 7293 4317 50  0000 C CNN
F 1 "LED" H 7293 4226 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 4100 50  0001 C CNN
F 3 "~" H 7300 4100 50  0001 C CNN
	1    7300 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 6084E795
P 7900 4100
F 0 "R10" V 7693 4100 50  0000 C CNN
F 1 "R" V 7784 4100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 4100 50  0001 C CNN
F 3 "~" H 7900 4100 50  0001 C CNN
	1    7900 4100
	0    1    1    0   
$EndComp
Text GLabel 8300 4100 2    50   3State ~ 0
D4
Wire Wire Line
	8300 4100 8050 4100
Wire Wire Line
	7450 4100 7750 4100
$Comp
L Device:LED D10
U 1 1 6084F142
P 7300 4400
F 0 "D10" H 7293 4617 50  0000 C CNN
F 1 "LED" H 7293 4526 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 4400 50  0001 C CNN
F 3 "~" H 7300 4400 50  0001 C CNN
	1    7300 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 6084F148
P 7900 4400
F 0 "R11" V 7693 4400 50  0000 C CNN
F 1 "R" V 7784 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 4400 50  0001 C CNN
F 3 "~" H 7900 4400 50  0001 C CNN
	1    7900 4400
	0    1    1    0   
$EndComp
Text GLabel 8300 4400 2    50   3State ~ 0
D3
Wire Wire Line
	8300 4400 8050 4400
Wire Wire Line
	7450 4400 7750 4400
$Comp
L Device:LED D11
U 1 1 6084F9CF
P 7300 4700
F 0 "D11" H 7293 4917 50  0000 C CNN
F 1 "LED" H 7293 4826 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 4700 50  0001 C CNN
F 3 "~" H 7300 4700 50  0001 C CNN
	1    7300 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 6084F9D5
P 7900 4700
F 0 "R12" V 7693 4700 50  0000 C CNN
F 1 "R" V 7784 4700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 4700 50  0001 C CNN
F 3 "~" H 7900 4700 50  0001 C CNN
	1    7900 4700
	0    1    1    0   
$EndComp
Text GLabel 8300 4700 2    50   3State ~ 0
D2
Wire Wire Line
	8300 4700 8050 4700
Wire Wire Line
	7450 4700 7750 4700
Wire Wire Line
	7150 1700 6900 1700
Wire Wire Line
	6900 1700 6900 2000
Wire Wire Line
	6900 4700 7150 4700
Wire Wire Line
	7150 4400 6900 4400
Connection ~ 6900 4400
Wire Wire Line
	6900 4400 6900 4700
Wire Wire Line
	6900 4100 7150 4100
Connection ~ 6900 4100
Wire Wire Line
	6900 4100 6900 4400
Wire Wire Line
	7150 3800 6900 3800
Connection ~ 6900 3800
Wire Wire Line
	6900 3800 6900 4100
Wire Wire Line
	6900 3500 7150 3500
Connection ~ 6900 3500
Wire Wire Line
	6900 3500 6900 3800
Wire Wire Line
	7150 3200 6900 3200
Connection ~ 6900 3200
Wire Wire Line
	6900 3200 6900 3500
Wire Wire Line
	6900 2900 7150 2900
Connection ~ 6900 2900
Wire Wire Line
	6900 2900 6900 3200
Wire Wire Line
	7150 2600 6900 2600
Connection ~ 6900 2600
Wire Wire Line
	6900 2300 7150 2300
Wire Wire Line
	6900 2300 6900 2600
Text GLabel 6900 5050 3    50   3State ~ 0
GND
Wire Wire Line
	6900 5050 6900 4700
Connection ~ 6900 4700
$Comp
L Device:Battery_Cell BT1
U 1 1 60883F5F
P 2450 5800
F 0 "BT1" V 2195 5850 50  0000 C CNN
F 1 "Battery_Cell" V 2286 5850 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" V 2450 5860 50  0001 C CNN
F 3 "~" V 2450 5860 50  0001 C CNN
	1    2450 5800
	0    1    1    0   
$EndComp
Text GLabel 2100 5800 0    50   Input ~ 0
GND
Wire Wire Line
	2100 5800 2350 5800
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60886519
P 4950 1450
F 0 "#FLG0101" H 4950 1525 50  0001 C CNN
F 1 "PWR_FLAG" H 4950 1623 50  0000 C CNN
F 2 "" H 4950 1450 50  0001 C CNN
F 3 "~" H 4950 1450 50  0001 C CNN
	1    4950 1450
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 60886999
P 5200 1450
F 0 "#FLG0102" H 5200 1525 50  0001 C CNN
F 1 "PWR_FLAG" H 5200 1623 50  0000 C CNN
F 2 "" H 5200 1450 50  0001 C CNN
F 3 "~" H 5200 1450 50  0001 C CNN
	1    5200 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 60886E41
P 5200 1450
F 0 "#PWR0101" H 5200 1300 50  0001 C CNN
F 1 "+5V" H 5215 1623 50  0000 C CNN
F 2 "" H 5200 1450 50  0001 C CNN
F 3 "" H 5200 1450 50  0001 C CNN
	1    5200 1450
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 60887D8B
P 5200 1900
F 0 "#PWR0102" H 5200 1750 50  0001 C CNN
F 1 "+5V" H 5215 2073 50  0000 C CNN
F 2 "" H 5200 1900 50  0001 C CNN
F 3 "" H 5200 1900 50  0001 C CNN
	1    5200 1900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 608889A4
P 4950 1450
F 0 "#PWR0103" H 4950 1200 50  0001 C CNN
F 1 "GND" H 4955 1277 50  0000 C CNN
F 2 "" H 4950 1450 50  0001 C CNN
F 3 "" H 4950 1450 50  0001 C CNN
	1    4950 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 60888A9A
P 4950 2000
F 0 "#PWR0104" H 4950 1750 50  0001 C CNN
F 1 "GND" H 4955 1827 50  0000 C CNN
F 2 "" H 4950 2000 50  0001 C CNN
F 3 "" H 4950 2000 50  0001 C CNN
	1    4950 2000
	1    0    0    -1  
$EndComp
Text GLabel 5200 1850 1    50   Input ~ 0
5V
Text GLabel 4950 1900 1    50   Input ~ 0
GND
Wire Wire Line
	4950 2000 4950 1900
Wire Wire Line
	5200 1900 5200 1850
$Comp
L Device:LED D2
U 1 1 608941A9
P 7300 2000
F 0 "D2" H 7293 2217 50  0000 C CNN
F 1 "LED" H 7293 2126 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 7300 2000 50  0001 C CNN
F 3 "~" H 7300 2000 50  0001 C CNN
	1    7300 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 608941AF
P 7900 2000
F 0 "R3" V 7693 2000 50  0000 C CNN
F 1 "R" V 7784 2000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7830 2000 50  0001 C CNN
F 3 "~" H 7900 2000 50  0001 C CNN
	1    7900 2000
	0    1    1    0   
$EndComp
Text GLabel 8300 2000 2    50   3State ~ 0
D11
Wire Wire Line
	8300 2000 8050 2000
Wire Wire Line
	7450 2000 7750 2000
Wire Wire Line
	7150 2000 6900 2000
Wire Wire Line
	6900 2000 6900 2300
Connection ~ 6900 2000
NoConn ~ 3400 1800
NoConn ~ 3600 1800
NoConn ~ 3500 1800
NoConn ~ 3400 2650
NoConn ~ 3200 2650
NoConn ~ 3100 2650
NoConn ~ 3000 2650
NoConn ~ 2900 2650
NoConn ~ 2800 2650
NoConn ~ 2700 2650
NoConn ~ 2600 2650
NoConn ~ 2500 2650
NoConn ~ 2400 2650
NoConn ~ 2300 2650
Wire Wire Line
	3600 2650 3600 2800
Wire Wire Line
	6900 2600 6900 2900
$EndSCHEMATC
