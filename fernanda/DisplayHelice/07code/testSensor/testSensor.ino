const int RETARDO = 50;
const int PIN_HALL = 13;

int HALL_VALUE;

void setup() {
  Serial.begin(19200);
  
  for(int i=2; i<=12; i++)
    pinMode(i, OUTPUT);

  pinMode(PIN_HALL, INPUT);
}


void loop() {
  HALL_VALUE = digitalRead(PIN_HALL);

  if(HALL_VALUE)
    ;
  else
    secuencia();
}

void secuencia(void){
  for(int i=2; i<=12; i++){
    digitalWrite(i, HIGH);
    delay(RETARDO);
  }

  for(int i=2; i<=12; i++){
    digitalWrite(i, LOW);
    delay(RETARDO);
  }
}
