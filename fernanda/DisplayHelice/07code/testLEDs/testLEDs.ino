const int RETARDO = 50;

void setup() {
  for(int i=2; i<=12; i++)
    pinMode(i, OUTPUT);
}

void loop() {
  for(int i=2; i<=12; i++){
    digitalWrite(i, HIGH);
    delay(RETARDO);
  }

  for(int i=2; i<=12; i++){
    digitalWrite(i, LOW);
    delay(RETARDO);
  }
}
