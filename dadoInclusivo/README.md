# Dado Inclusivo con Diseño Universal para PcD

En la actualidad los derechos de las Personas con Discapacidad están reconocidas constitucionalmente por medio de la Convención Sobre los Derechos de las Personas con Discapacidad.

---

Para lograr la inclusión de las personas con discapacidad es importante que puedan participar en igualdad de circunstancias en todos los ámbitos de la vida, eso incluye los espacios de dispersión y convivencia por medio de los juegos de mesa.

---

En la actualidad existen pocos juegos de mesa que sean inclusivos pensados desde el diseño universal.

---

Para favorecer este proceso nos proponemos generar un Dado Inclusivo atendiendo el Diseño Universal.

---

# Planeación

---

A continuación se enumeran las relaciones del dado con el algunas de las discapacidades.

----

## Discapacidad Visual

Las personas con baja visión o ceguera tienen dificultad para ver la cara en la que el dado ha caído. Aún cuando los puntitos de un dado estándar están en bajorelieve esto dificulta su lectura ya que en general las PcD están más habituadas al alto relieve del sistema de lectoescritura Braille.

---

## Discapacidad Auditiva

En el caso de la discapacidad auditiva es posible ver qué numero cayó el dado. En ocasiones este cae fuera del tablero o incluso de la mesa de juego, en esos casos la vista puede ayudar a encontrar o distinguir el dado de forma más rápida.

----

## Discapacidad Psicosocial

_To do_

----

## Discapacidad Física

Las diversas manifestaciones de la discapacidad física nos llevan a sugerir formas alternas para la activación del dado, estas pueden ser por movimiento o por un botón de activación.

---

## Discapacidad Intelectual

Para incentivar la retención del número en la memoria de las personas se puede recurrir a la repetición de la información tanto en lo auditivo como en lo visual.

---

# Propuesta de salida

---

![](https://codimd.s3.shivering-isles.com/demo/uploads/upload_0fa58ff18ee3a3966e2a5924dd728727.jpeg)

Imagen de la propuesta de salida

----

# Diseño electrónico

El diseño electrónico se desarrolló con KiCAD y está publicado
bajo la licencia Creative Commons.

La versión más reciente se encuentra en la carpeta [V1.0/kicad](V1.0/kicad).

----

## PCB

El diseño del PCB  desarrolló con KiCAD y está publicado bajo la licencia Creative Commons.

La versión más reciente se encuentra en la carpeta [V1.0/kicad](V1.0/kicad).
Esta versión esta en dos capas lista para su maquila a nivel profesional.

Si deseas hacer una version casera te recomendamos la versión 0.2 del dado multifuncional, en la carpeta [V0.2/kicad](V0.2/kicad).

----

## Código

El código puede encontrarse en la carpeta [V1.0/code)](V1.0/code)

---

# Potencial Educativo

**Nivel Básico:** Ensamble y montaje en case
**Nivel Intermedio:** Soldadura SMD
**Nivel Avansado:** Diseño electrónico

---

# Contribuciones

Este proyecto es Hardware y Software Libre, bajo la licencia Creative Commons.
Son bienvenidas todas las aportaciones tanto en código como en hardware via gitlab.

# Contacto:

* **Pavel E. vazquez Mtz** electroemprende19[at]riseup.net
* **Esteban Estrada Domínguez:**
* **Fernando Vázquez Díaz:**
