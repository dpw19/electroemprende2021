
# V1.0
**Date:** 19/11/21
**Descripción:**
* Se reacomodan los componentes para dos capas
* Dimensiónes finales 35mm x 30mm
* Se cambian los LEDs a SMD para utilizar dos capas
* Se cambia el conector SPI de 3x2 a 1x6

# V0.2
**Date:** 09/11/21
**Descripción:**
* Se corrige el footprint del cristal de cuarzo
* Se reemplazan los headers THT por headers SMD

# V0.1
**Date:** 14/10/21
**Descripción:**
* La version V0.1 es el primer prototipo.
* Aún no esta adaptada a las formas y dimensiones de las cajas.
* Los leds son THT y los demás componentes son SMD.
* Sirve para probar la probramación vía SPI, desarrollo de código y practica 
de soladura con componentes SMD.


