/*
 * Dado inclusivo V0.0
*/

const int retardo = 2000;

const int LED1 = 6;
const int LED2 = 5;
const int LED3 = 4;
const int LED4 = 10;
const int LED5 = 9;
const int LED6 = 8;
const int LED7 = 7;

void setup() {
   pinMode(LED1, OUTPUT);
   pinMode(LED2, OUTPUT);
   pinMode(LED3, OUTPUT);
   pinMode(LED4, OUTPUT);
   pinMode(LED5, OUTPUT);
   pinMode(LED6, OUTPUT);
   pinMode(LED7, OUTPUT);
}

void loop() {
  apagado();
  uno();
  delay(retardo);

  apagado();
  dos();
  delay(retardo);

  apagado();
  tres();
  delay(retardo);

  apagado();
  cuatro();
  delay(retardo);

  apagado();
  cinco();
  delay(retardo);

  apagado();
  seis();
  delay(retardo);
}

void uno(void){
  digitalWrite(LED7, HIGH);
}

void dos(void){
  digitalWrite(LED1, HIGH);
  digitalWrite(LED6, HIGH);  
}

void tres(void){
  digitalWrite(LED1, HIGH);
  digitalWrite(LED6, HIGH);
  digitalWrite(LED7, HIGH);
}

void cuatro(void){
  digitalWrite(LED1, HIGH);
  digitalWrite(LED3, HIGH);
  digitalWrite(LED4, HIGH);
  digitalWrite(LED6, HIGH);  
}

void cinco(void){
  digitalWrite(LED1, HIGH);
  digitalWrite(LED3, HIGH);
  digitalWrite(LED4, HIGH);
  digitalWrite(LED6, HIGH);
  digitalWrite(LED7, HIGH);  
}

void seis(void){
  digitalWrite(LED1, HIGH);
  digitalWrite(LED2, HIGH);
  digitalWrite(LED3, HIGH);
  digitalWrite(LED4, HIGH);
  digitalWrite(LED5, HIGH);
  digitalWrite(LED6, HIGH);
}

void apagado(void){
  digitalWrite(LED1, LOW);
  digitalWrite(LED2, LOW);
  digitalWrite(LED3, LOW);
  digitalWrite(LED4, LOW);
  digitalWrite(LED5, LOW);
  digitalWrite(LED6, LOW);
  digitalWrite(LED7, LOW);
}
