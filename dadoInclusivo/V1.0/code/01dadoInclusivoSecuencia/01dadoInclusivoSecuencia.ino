/*
 * Dado inclusivo V0.0
*/
const int retardo = 1000;

const int LED1 = 6;
const int LED2 = 5;
const int LED3 = 4;
const int LED4 = 10;
const int LED5 = 9;
const int LED6 = 8;
const int LED7 = 7;

void setup() {
   pinMode(LED1, OUTPUT);
   pinMode(LED2, OUTPUT);
   pinMode(LED3, OUTPUT);
   pinMode(LED4, OUTPUT);
   pinMode(LED5, OUTPUT);
   pinMode(LED6, OUTPUT);
   pinMode(LED7, OUTPUT);
}

void loop() {
  digitalWrite(LED1, HIGH);
  delay(retardo);
  digitalWrite(LED1, LOW);
  
  digitalWrite(LED2, HIGH);
  delay(retardo);
  digitalWrite(LED2, LOW);

  digitalWrite(LED3, HIGH);
  delay(retardo);
  digitalWrite(LED3, LOW);

  digitalWrite(LED4, HIGH);
  delay(retardo);
  digitalWrite(LED4, LOW);

  digitalWrite(LED5, HIGH);
  delay(retardo);
  digitalWrite(LED5, LOW);

  digitalWrite(LED6, HIGH);
  delay(retardo);
  digitalWrite(LED6, LOW);

  digitalWrite(LED7, HIGH);
  delay(retardo);
  digitalWrite(LED7, LOW);  
}
