#include "pitches.h"

const int retardo = 1000;

const int LED1 = 6;
const int LED2 = 5;
const int LED3 = 4;
const int LED4 = 10;
const int LED5 = 9;
const int LED6 = 8;
const int LED7 = 7;

const int BOTON=3;

const int BUZZER = 18; // pin del buzzer

int LED;  // led para encender dentro de las melodias
int numero; 
// Melodia 1
int melody1[] = {
C5,C5,G5,G5,A5,A5,G5,F5,F5,E5,E5,D5,D5,C5,G5,G5,F5,F5,E5,E5,D5,G5,G5,F5,F5,E5,E5,D5
};
int noteDurations1[] = {
  4, 4, 4, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 2
};

// Melodia 2
int melody2[] = {
  G5, G5, F5, F5, E5, E5, D5
};
int noteDurations2[] = {
  4, 4, 4, 4, 4, 4, 2
};


void setup() {
  pinMode(BOTON,INPUT);
  
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  pinMode(LED5, OUTPUT);
  pinMode(LED6, OUTPUT);
  pinMode(LED7, OUTPUT);

  randomSeed(analogRead(0));
  tocarMelodia1();
}

void loop() {
  if(!digitalRead(BOTON)){
    delay(50);
    if(!digitalRead(BOTON)){
      tocarMelodia2();
    
      delay(200);
    
      switch(random(1,7)){
        case 1:
          uno();
          break;
        case 2:
          dos();
          break;
        case 3:
          tres();
          break;
        case 4:
          cuatro();
          break;
        case 5:
          cinco();
          break;
        case 6:
          seis();
          break;
      } 
    }
  }
}

void uno(void){
  for(int i = 0 ; i <= 1; i++){
    digitalWrite(LED7, HIGH); 
    tone(BUZZER, C5);
    delay(400);
    digitalWrite(LED7, LOW);
    noTone(BUZZER);
  }
}

void dos(void){
  for(int i = 0 ; i <= 2-1; i++){
    digitalWrite(LED1, HIGH);
    digitalWrite(LED6, HIGH);
    tone(BUZZER, C5);
    delay(400);
    noTone(BUZZER); 
    apagado();  
    delay(200);
  }    
  
}

void tres(void){
  for(int i = 0 ; i <= 3-1; i++){
    digitalWrite(LED1, HIGH);
    digitalWrite(LED6, HIGH);
    digitalWrite(LED7, HIGH);
    tone(BUZZER, C5);
    delay(400);
    noTone(BUZZER);
    apagado();
    delay(200);
  }
}

void cuatro(void){
  for(int i=0; i<=4-1; i++){
    digitalWrite(LED1, HIGH);
    digitalWrite(LED3, HIGH);
    digitalWrite(LED4, HIGH);
    digitalWrite(LED6, HIGH);
    tone(BUZZER,C5);
    delay(400);
    noTone(BUZZER);
    apagado();
    delay(200);
  }
}

void cinco(void){
  for(int i=0; i <= 5-1; i++){
    digitalWrite(LED1, HIGH);
    digitalWrite(LED3, HIGH);
    digitalWrite(LED4, HIGH);
    digitalWrite(LED6, HIGH);
    digitalWrite(LED7, HIGH);  
    tone(BUZZER,C5);
    delay(400);
    noTone(BUZZER);
    apagado();
    delay(200);
  }
}

void seis(void){
  for(int i=0; i <= 6-1; i++){
     digitalWrite(LED1, HIGH);
     digitalWrite(LED2, HIGH);
     digitalWrite(LED3, HIGH);
     digitalWrite(LED4, HIGH);
     digitalWrite(LED5, HIGH);
     digitalWrite(LED6, HIGH);
     tone(BUZZER,C5);
     delay(400);
     noTone(BUZZER);
     apagado();
     delay(200);
  }
}

void apagado(void){
  digitalWrite(LED1, LOW);
  digitalWrite(LED2, LOW);
  digitalWrite(LED3, LOW);
  digitalWrite(LED4, LOW);
  digitalWrite(LED5, LOW);
  digitalWrite(LED6, LOW);
  digitalWrite(LED7, LOW);
}

void tocarMelodia1(){
  for (int thisNote = 0; thisNote < sizeof(melody1)/sizeof(int); thisNote++) {
    LED = random(4,11);
    digitalWrite(LED, HIGH);
    int noteDuration = 800 / noteDurations1[thisNote];
    tone(BUZZER, melody1[thisNote], noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noTone(BUZZER);
    digitalWrite(LED, LOW);
  }
}

void tocarMelodia2(){
  for (int thisNote = 0; thisNote < sizeof(melody2)/sizeof(int); thisNote++) {
    LED = random(4,11);
    digitalWrite(LED, HIGH);
    int noteDuration = 800 / noteDurations2[thisNote];
    tone(BUZZER, melody2[thisNote], noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noTone(BUZZER);
    digitalWrite(LED, LOW);
  }
}
