# Electroemprende 2021

## Objetivo

Las personas participantes desarrollarán, implementarán y darán seguimiento a un proyecto que genere productos o servicios mediante el estudio y aplicación de la electrónica, fomentando así la autonomía económica.

## Perfil

Si bien el taller está abierto a todas las personas interesadas será más fácil para ti si tienes el siguiente perfil:

* Gusto por el trabajo colaborativo
* Disposición
* Iniciativa e inventiva
* Deseo de adentrarse al mundo de la electrónica
* Deseo/gusto de emprender

## Flujo de Trabajo

Para alcanzar los objetivos del taller proponemos trabajar de forma paralela en un proyecto de emprendimiento de productos o servicios, a la vez que se avanza en los conocimientos de la electrónica.

De esta forma se pueden aplicar los conocimientos generados en forma práctica.

Es importante que pienses en algo que desees realizar y que tenga relación con la electrónica, en este momento no importa que aún no tengas conocimientos previos en la materia, ya que los módulos consideran una sección teórica y contarás con acompañamiento para aterrizar los conocimientos aplicados a tu proyecto.

## Permanencia en el taller

Para considerar tu permanencia en el taller es importante que participes activamente en los siguientes aspectos:

* Pensar en al menos un producto o un servicio para su emprendimiento.
* Realizar las actividades propuestas en la plataforma de educación asóncrona.
* Dar seguimiento a los avances de tus propuestas.
* Realizar y presentar avances, prototipos que se aproximen sucesivamente a una conclusión del proyecto.
* Retroalimentar y dar seguimiento a otros proyetos.

¡El trabajo colaborativo beneficia a todas las personas!

## Inscripciones

~~Para inscribirte al taller hay que llenar el 
[formulario](https://edupanti.org/forms/index.php/878114?lang=es-MX)~~


## Medios de contacto

### Seguimiento a personas inscritas

* La comunicación directa se hace por el grupo de [Telegram](telegram.org).

Puedes obtener el enlace algrupo al realizar tu registro.

* Las videocharlas se realizan via [meet.jit.si](meet.jit.si).

El enlace se comparte en el grupo de Télegram.

* Las actividades en línea se realizan en [edupanti.org/educacion](https://edupanti.org/educacion)

## Horarios

### Teoría

Lunes y Miércoles 10 a 12 horas.

### Proyectos

Martes y Jueves 10 a 12 horas.

### Asesorias

Por el grupo de Telegram, cuando sean necesarias.

### Teoría

De forma asíncrona via [edupanti.org/educacion](https://edupanti.org/educacion) y de forma semipresencial.


